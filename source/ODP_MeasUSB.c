#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "LPC55S69_cm33_core0.h"
#include "fsl_debug_console.h"
#include "usb_device_interface_0_cic_vcom.h"
#include "fsl_power.h"
#include "lcd.h"
#include "back.h"
#include "arm_math.h"

#define FS	96000
#define MAXSET 300
#define MAXPOS 12300
#define CTIMER_MAT_PWM_PERIOD_CHANNEL kCTIMER_Match_3

volatile int32_t Set=0, motorPower=30;
volatile int32_t Position=0, oldPosition=0, divPosition=0, setPosition=0;
volatile uint8_t divInterval=0;
volatile bool Run=false, posReached=false, forward=true, backward=true;
uint16_t modeColor=0;
volatile float32_t Speed=0;
arm_pid_instance_f32 Coef;
volatile float32_t e=0, x=0;
extern uint32_t *data1;
volatile uint32_t g_systickCounter;
int counter=0;

void SysTick_Handler(void)
{
	if (g_systickCounter)
		g_systickCounter--;
}

void Motor_SetPower(int32_t power, bool brake);

void Dir_change_R_callback(pint_pin_int_t pintr ,uint32_t pmatch_status){

	motorPower*=(-1);
}
void Dir_change_L_callback(pint_pin_int_t pintr ,uint32_t pmatch_status){

	motorPower*=(-1);
}
void cbEncoder_Rotate(pint_pin_int_t pintr ,uint32_t pmatch_status)
{
	if(GPIO_PinRead(BOARD_INITPINS_ENC_A_GPIO, BOARD_INITPINS_ENC_A_PORT, BOARD_INITPINS_ENC_A_PIN)) {
		if(Set < MAXSET)
			Set++;
	}
	else{
		if(Set > -MAXSET)
			Set--;
	}
}

void cbEncoder_Push(pint_pin_int_t pintr ,uint32_t pmatch_status)
{
	Run=!Run;
}

void cbMotor_Rotate(pint_pin_int_t pintr ,uint32_t pmatch_status)
{
	if(GPIO_PinRead(BOARD_INITPINS_MOT_ENCA_GPIO, BOARD_INITPINS_MOT_ENCA_PORT, BOARD_INITPINS_MOT_ENCA_PIN))
		Position++;
	else
		Position--;
}

void cbMotor_PWM(uint32_t flags)
{
	divInterval++;
	if(divInterval>=100){
		divInterval=0;
		divPosition=(Position-oldPosition);
		oldPosition=Position;
		Speed=divPosition*1.25;


	}
}

void Motor_SetPower(int32_t power, bool brake)
{

	if(power > 100)
		power = 100;
	if(power < -100)
		power = -100;
	if(power>0) {
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRA_GPIO, BOARD_INITPINS_MOT_DIRA_PORT, BOARD_INITPINS_MOT_DIRA_PIN, 0);
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRB_GPIO, BOARD_INITPINS_MOT_DIRB_PORT, BOARD_INITPINS_MOT_DIRB_PIN, 1);
	}
	else if(power<0) {
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRA_GPIO, BOARD_INITPINS_MOT_DIRA_PORT, BOARD_INITPINS_MOT_DIRA_PIN, 1);
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRB_GPIO, BOARD_INITPINS_MOT_DIRB_PORT, BOARD_INITPINS_MOT_DIRB_PIN, 0);
	}
	else {
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRA_GPIO, BOARD_INITPINS_MOT_DIRA_PORT, BOARD_INITPINS_MOT_DIRA_PIN, brake);
		GPIO_PinWrite(BOARD_INITPINS_MOT_DIRB_GPIO, BOARD_INITPINS_MOT_DIRB_PORT, BOARD_INITPINS_MOT_DIRB_PIN, brake);
	}
	CTIMER_UpdatePwmDutycycle(CTIMER2_PERIPHERAL, CTIMER_MAT_PWM_PERIOD_CHANNEL, CTIMER2_PWM_1_CHANNEL, abs(power));
}

void delay_ms(uint32_t n)
{
	g_systickCounter = n;
	while (g_systickCounter);
}



void manualControl(){
	setPosition=(MAXPOS/100)*data1[1];
	if(posReached==true){
		LCD_Puts(10, 48, "Set :", 0x0FFF);
		LCD_7seg(34, 48, data1[1], 4, 0x0FFF);
		LCD_Puts(134, 72, "%", 0x0FFF);

		LCD_Puts(10, 90, "Curr. pos", 0x0FFF);
		LCD_7seg(34, 90, Position, 4, 0x0FFF);
	}
	else{
		if(Position<setPosition){

			Motor_SetPower(motorPower, 1);
			Run=true;
		}
		else if (Position==setPosition){
			Motor_SetPower(0,0);
			posReached=true;
		}
		else if(Position>setPosition){
			Motor_SetPower(-motorPower, 1);

		}
	}
}

void continousWork(){

	if(Position<(MAXPOS/2)+((MAXPOS/200)*data1[1]) && forward==true){
		Motor_SetPower(motorPower, 1);
	}

	else if (Position==(MAXPOS/2)+((MAXPOS/200)*data1[1])){
		forward= false;

	}

	else if(Position>(MAXPOS/2)-((MAXPOS/200)*data1[1])){
		Motor_SetPower(-motorPower, 1);
	}

	else if (Position<=(MAXPOS/2)-((MAXPOS/200)*data1[1])){
		forward=true;
		Position=(MAXPOS/2)-((MAXPOS/200)*data1[1]);
	}
}
int i=1;

void stepWork(){
	setPosition=50;

	if(i<=data1[1]){

		if(Position<setPosition){
			Motor_SetPower(motorPower, 1);
			Run=true;
		}
		else{
			Motor_SetPower(0, 0);
			delay_ms(data1[2]*1000);
			i++;
			Position=0;
		}

	}
	else {
		Motor_SetPower(0,0);
		LCD_Puts(10, 48, "Set :", 0x0FFF);
		LCD_7seg(34, 48, data1[1], 4, 0x0FFF);
		LCD_Puts(134, 72, "%", 0x0FFF);

		LCD_Puts(10, 90, "Curr. pos", 0x0FFF);
		LCD_7seg(34, 90, Position, 4, 0x0FFF);
	}

}


int main(void) {

	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();
#ifndef BOARD_INIT_DEBUG_CONSOLE_PERIPHERAL
	/* Init FSL debug console. */
	BOARD_InitDebugConsole();
#endif

	LCD_Init(FLEXCOMM3_PERIPHERAL);
	EnableIRQ(CTIMER2_TIMER_IRQN);
	SysTick_Config(SystemCoreClock / 1000U);

	while(1) {

		USB_DeviceInterface0CicVcomTask();
		LCD_Set_Bitmap((uint16_t*)back_160x128);

		LCD_Puts(10, 6, "Mode", 0x0FFF);
		LCD_7seg(34, 6, data1[0], 2, 0x0FFF);

		switch (data1[0]){

		case 1:
			manualControl();
			LCD_Puts(10, 48, "Set :", 0x0FFF);
			LCD_7seg(34, 48, data1[1], 4, 0x0FFF);
			LCD_Puts(134, 72, "%", 0x0FFF);

			LCD_Puts(10, 90, "Curr. pos", 0x0FFF);
			LCD_7seg(34, 90, Position, 4, 0x0FFF);
			break;

		case 2:
			continousWork();
			LCD_Puts(10, 48, "Range of motion:", 0x0FFF);
			LCD_7seg(34, 48, counter, 4, 0x0FFF);
			LCD_Puts(134, 72, "%", 0x0FFF);

			LCD_Puts(10, 90, "Speed:", 0x0FFF);
			LCD_7seg(34, 90, Position, 4, 0x0FFF);
			LCD_Puts(134,114, "%", 0x0FFF);
			break;

		case 3:
			stepWork();
			LCD_Puts(10, 48, "Number of steps:", 0x0FFF);
			LCD_7seg(34, 48, data1[1], 4, 0x0FFF);

			LCD_Puts(10, 90, "Time interval:", 0x0FFF);
			LCD_7seg(34, 90, data1[2], 4, 0x0FFF);
			LCD_Puts(134,114, "s", 0x0FFF);
			break;
		}

		LCD_GramRefresh();
	}
	return 0 ;
}
