################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../usb/device/class/usb_device_class.c 

OBJS += \
./usb/device/class/usb_device_class.o 

C_DEPS += \
./usb/device/class/usb_device_class.d 


# Each subdirectory must supply rules for building sources it contributes
usb/device/class/%.o: ../usb/device/class/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_LPC55S69JBD100 -DCPU_LPC55S69JBD100_cm33 -DCPU_LPC55S69JBD100_cm33_core0 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DSERIAL_PORT_TYPE_UART=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\drivers" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\LPC55S69\drivers" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\utilities" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\serial_manager" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\lists" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\uart" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\class\cdc" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\output\source\device\class" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\class" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\source" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\output\source\device" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\osa" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\include" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\source\lpcip3511" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\include" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\phy" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\source\generated" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\CMSIS" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\device" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\board" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\source" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\drivers" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\device" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\CMSIS" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\utilities" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\serial_manager" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\lists" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\uart" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\class\cdc" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\class" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\source" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\component\osa" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\include" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\device\source\lpcip3511" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\source\generated" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\include" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\usb\phy" -I"D:\Studia\MCUXpressoIDE_11.3.0_5222\Workspace\Linear_guide\startup" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m33 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


